from django.contrib import admin
from django.conf import settings
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('geo.map.api.urls')),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns.append(
        path('debug/', include(debug_toolbar.urls))
    )
