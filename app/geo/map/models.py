import uuid
from django.utils.translation import gettext_lazy as _
from django.contrib.gis.db import models


scheam = 'content\".\"'


class TimeStampedMixin(models.Model):

    created = models.DateTimeField(auto_now_add=True, null=True)
    modified = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True

class UUIDMixin(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    class Meta:
        abstract = True


class Country(UUIDMixin, TimeStampedMixin):

    name = models.CharField(_('name'), max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        db_table = scheam + 'country'
        verbose_name = _('country')
        verbose_name_plural = _('countrys')


class Subject(UUIDMixin, TimeStampedMixin):

    name = models.CharField(_('name'), max_length=255)
    geometry = models.GeometryField(default=None)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        db_table = scheam + 'subject'
        verbose_name = _('subject')
        verbose_name_plural = _('subject')


class PointTask(UUIDMixin):

    point = models.PointField(default=None)
    visited = models.DateTimeField(auto_now_add=True, null=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {} {}'.format(self.point, self.visited, self.subject)

    class Meta:
        db_table = scheam + 'point_task'
        verbose_name = _('point_task')
        verbose_name_plural = _('point_task')
