from django.contrib.gis import admin as geo_admin
from .models import Subject, Country, PointTask
from django.contrib import admin


@admin.register(Country)
class CountryAdmin(geo_admin.OSMGeoAdmin):
    list_display = ('name',)


@admin.register(Subject)
class RegionAdmin(geo_admin.OSMGeoAdmin):
    list_display = ('name', 'country')
    list_filter = ('country',)
    map_width = 700
    map_height = 300

    def get_name(self, obj):
        return obj.country.name


@admin.register(PointTask)
class RegionAdmin(geo_admin.OSMGeoAdmin):
    list_display = ('point', 'visited', 'subject')
    list_filter = ('subject',)

    def get_name(self, obj):
        return obj.subject.name
