from django.http import JsonResponse
from django.views.generic.list import BaseListView
from django.views.generic.detail import BaseDetailView

from geo.map.models import Subject

class RegionsApiMixin:
    model = Subject
    http_method_names = ['get']

    def render_to_response(self, context, **response_kwargs):
        return JsonResponse(context)


class RegionsListApi(RegionsApiMixin, BaseListView):

    paginate_by = 50

    def get_queryset(self):
        qs = self.model.objects.prefetch_related('region')

        qs = qs.values(
            'id', 'name', 'geometry'
        )
        return qs

    def get_context_data(self, *, object_list=None, **kwargs):
        queryset = self.get_queryset()
        paginator, page, queryset, is_paginated = self.paginate_queryset(
            queryset,
            self.paginate_by
        )
        prev = page.previous_page_number() if page.has_previous() else None
        next = page.next_page_number() if page.has_next() else None

        return {
            'count': paginator.count,
            'total_pages': paginator.num_pages,
            'prev': prev,
            'next': next,
            'results': list(queryset)
        }


class RegionsDetailApi(RegionsApiMixin, BaseDetailView):

    def get_queryset(self):
        qs = self.model.objects.filter(pk=self.kwargs['pk'])
        qs = qs.values(
            'id', 'name'
        )
        return qs

    def get_context_data(self, **kwargs):
        return self.get_object(queryset=self.get_queryset())