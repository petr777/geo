from django.urls import path
from geo.map.api.v1 import views

urlpatterns = [
    path('regions/', views.RegionsListApi.as_view()),
    path('regions/<uuid:pk>/', views.RegionsDetailApi.as_view())
]