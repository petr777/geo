from django.urls import path, include

urlpatterns = [
    path('v1/', include('geo.map.api.v1.urls')),
]