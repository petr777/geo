# Geo


## Getting started

Установка Django и батареек

```bash
poetry add django = "4.0.3"
poetry add django-split-settings = "1.1.0"
poetry add django-debug-toolbar = "3.2.4"
poetry add django-admin-tools = "0.9.2"
```

Создаем проект текущей директории django командой
```bash
django-admin startproject config .
```

Для удобства хранения настроеек воспольземся `django-split-settings`
в директории `config` создадим директорию `components` \
в которой создадим `database.py` и `geo.py`\
тут будем храниться настройки для `PostGis` и для `GeoDjango` соответственно.


`postgis.py`
```python
import os

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': os.environ.get('POSTGRES_HOST', '127.0.0.1'),
        'PORT': os.environ.get('POSTGRES_PORT', 5432),
        'OPTIONS': {
            'options': '-c search_path=public,content'
        },
    }
}
```

`geodjango.py`
```python
import os

if os.name == 'nt':
    import platform
    OSGEO4W = r"C:\OSGeo4W"
    if '64' in platform.architecture()[0]:
        OSGEO4W += "64"
    assert os.path.isdir(OSGEO4W), "Directory does not exist: " + OSGEO4W
    os.environ['OSGEO4W_ROOT'] = OSGEO4W
    os.environ['GDAL_DATA'] = OSGEO4W + r"\share\gdal"
    os.environ['PROJ_LIB'] = OSGEO4W + r"\share\proj"
    os.environ['PATH'] = OSGEO4W + r"\bin;" + os.environ['PATH']
```

В основном файле `settings` подключаем `database.py` и `geo.py`
`settings.py`

```python
from dotenv import load_dotenv
from split_settings.tools import include

load_dotenv()
include(
    'components/database.py',
    'components/geo.py',
)
```

Для работы GeoDjango необходимо:

1. в `INSTALLED_APPS` прописать
```bash
INSTALLED_APPS = [
    ...
    'django.contrib.gis',
]
```

Создаем директорию `geo`, где будем хранить первый модули приложения django 
```bash
mkdir geo/map
```

Генерируем стартовую структур модуля приложения
```bash
django-admin startapp map ./geo/map
```

Подключаем наш модуль в `INSTALLED_APPS`
```bash
INSTALLED_APPS = [
    ...
    'geo.map'
]
```